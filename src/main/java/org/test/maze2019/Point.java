package org.test.maze2019;

import java.util.ArrayList;
import java.util.List;

public final class Point {

	private final int x;

	private final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
		
	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		if(obj.getClass() != Point.class) {
			return false;
		}
		final Point p = (Point) obj;
		return x == p.x && y == p.y;
	}
	
	@Override
	public int hashCode() {
		return x ^ y;
	}
	
	public static List<Point> siblings(Point point, int offset) {
		final ArrayList<Point> list = new ArrayList<Point>(4);
		list.add(new Point(point.x + offset, point.y));
		list.add(new Point(point.x, point.y + offset));
		list.add(new Point(point.x - offset, point.y));
		list.add(new Point(point.x, point.y - offset));
		return list;
	}
}
