package org.test.maze2019;

public class Main {

	private static final int SIZE = 24;

	public static void main(String[] args) {
		int width = SIZE * 2 + 1;
		int height = SIZE * 2 + 1;
		
		final Maze maze = new Maze(width, height);
		final Path path = Path.find(maze);
				
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Cell type = path.get(x, y);
				if(type == null) {
					type = maze.get(x, y);
				}
				switch (type) {
				case floor:
					System.out.append('0');
					break;
				case wall:
					System.out.append('1');
					break;
				case path:
					System.out.append('*');
					break;
				default:
					break;
				}
			}
			System.out.println();
		}
		System.out.flush();
	}
}
