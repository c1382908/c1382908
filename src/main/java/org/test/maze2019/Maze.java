package org.test.maze2019;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;

public class Maze {

	private static final Random RND = new Random();

	private final Cell[] data;

	private final int width;

	private final int height;

	private Point start;

	private Point end;

	public Maze(int width, int height) {
		this.width = width;
		this.height = height;
		this.data = new Cell[width * height];
		
		prepare();
	}

	public Cell get(Point cell) {
		return data[cell.getX() + cell.getY() * width];
	}
	
	public Cell get(int x, int y) {
		return data[x + y * width];
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return end;
	}

	private void prepare() {
		final HashSet<Point> visited = new HashSet<Point>();
		final LinkedList<Point> buffer = new LinkedList<>();
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if ((y < height - 1 && x < width - 1) && (y % 2 != 0 && x % 2 != 0)) {
					data[x + y * width] = Cell.floor;
				} else {
					data[x + y * width] = Cell.wall;
				}
			}
		}
		
		start = new Point(RND.nextInt((width - 1) / 2) * 2 + 1, 0);
		end = new Point(RND.nextInt((width - 1) / 2) * 2 + 1, height - 1);
		data[start.getX() + start.getY() * width] = Cell.floor;
		data[end.getX() + end.getY() * width] = Cell.floor;
		

		buffer.push(new Point(1, 1));
		while (!buffer.isEmpty()) {
			final Point cell = buffer.peek();
			visited.add(cell);

			final Point next = findNext(visited, cell);
			if (next == null) {
				buffer.pop();
			} else {
				buffer.push(next);
				
				final int dx = next.getX() - cell.getX();
				final int dy = next.getY() - cell.getY();
				final int x = cell.getX() + (dx != 0 ? dx / Math.abs(dx) : 0);
				final int y = cell.getY() + (dy != 0 ? dy / Math.abs(dy) : 0);
				
				data[x + y * width] = Cell.floor;
			}
		}
	}

	private Point findNext(Set<Point> visisted, Point cell) {
		ArrayList<Point> buffer = new ArrayList<Point>();
		for (Point c : Point.siblings(cell, 2)) {
			if (c.getX() > 0 && c.getX() < width && c.getY() > 0 && c.getY() < height) {
				if (!visisted.contains(c)) { 
					buffer.add(c);
				}
			}
		}
		if (buffer.isEmpty()) {
			return null;
		} else {
			return buffer.get(RND.nextInt(buffer.size()));
		}
	}
}
