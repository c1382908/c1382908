package org.test.maze2019;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

public class Path {

	private final HashSet<Point> value;

	public static Path find(Maze maze) {
		final int width = maze.getWidth();
		final int height = maze.getHeight();
		
		final HashSet<Point> visisted = new HashSet<>();
		
		final LinkedList<Point> stack = new LinkedList<>();
		stack.push(maze.getStart());
				
		while (!stack.peek().equals(maze.getEnd())) {
			final Point point = stack.peek();
			visisted.add(point); 	
			
			Point next = null;
			for (Point p : Point.siblings(point, 1)) {
				if (p.getX() > 0 && p.getX() < width && p.getY() > 0 && p.getY() < height) {
					if(!visisted.contains(p) && maze.get(p) == Cell.floor) {
						next = p;
						break;	
					}
				}
			}
			if(next == null) {
				stack.pop();
			} else {
				stack.push(next);
			}
		} 
		return new Path(stack);
	}
	
	public Path(Collection<Point> source) {
		value = new HashSet<Point>(source);
	}
	
	public Cell get(int x, int y) {
		return value.contains(new Point(x, y)) ? Cell.path : null;
	}
}
